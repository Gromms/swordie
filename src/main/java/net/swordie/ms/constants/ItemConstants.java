package net.swordie.ms.constants;

import net.swordie.ms.ServerConstants;
import net.swordie.ms.client.character.items.*;
import net.swordie.ms.connection.db.DatabaseManager;
import net.swordie.ms.enums.*;
import net.swordie.ms.life.drop.DropInfo;
import net.swordie.ms.life.pet.PetSkill;
import net.swordie.ms.loaders.ItemData;
import net.swordie.ms.loaders.containerclasses.EquipDrop;
import net.swordie.ms.loaders.containerclasses.ItemInfo;
import net.swordie.ms.util.Util;
import org.apache.log4j.LogManager;

import java.util.*;
import java.util.stream.Collectors;

import static net.swordie.ms.enums.InvType.EQUIP;

/**
 * Created on 12/12/2017.
 */
public class ItemConstants {
    public static final int EMPTY_SOCKET_ID = 3;
    public static final short INACTIVE_SOCKET = 0;
    public static final short MIN_LEVEL_FOR_SOUL_SOCKET = 75;
    public static final int SOUL_ENCHANTER_BASE_ID = 2590000;
    public static final int SOUL_ITEM_BASE_ID = 2591000;
    public static final int MAX_SOUL_CAPACITY = 1000;
    public static final int MOB_DEATH_SOUL_MP_COUNT = 150;
    public static final int MOB_CARD_BASE_ID = 2380000;
    public static final int FAMILIAR_PREFIX = 996;
    public static final int SPELL_TRACE_ID = 4001832;
    public static final int RAND_CHAOS_MAX = 5;
    public static final int INC_RAND_CHAOS_MAX = 10;

    public static final byte MAX_SKIN = 13;
    public static final int MIN_HAIR = 30000;
    public static final int MAX_HAIR = 49999;
    public static final int MIN_FACE = 20000;
    public static final int MAX_FACE = 29999;

    static final org.apache.log4j.Logger log = LogManager.getRootLogger();

    public static final int THIRD_LINE_CHANCE = 50;
    public static final int PRIME_LINE_CHANCE = 15;

    public static final int HYPER_TELEPORT_ROCK = 5040004;

    public static final int RED_CUBE = 5062009;
    public static final int BLACK_CUBE = 5062010;

    public static final int BONUS_POT_CUBE = 5062500;
    public static final int SPECIAL_BONUS_POT_CUBE = 5062501;
    public static final int WHITE_BONUS_POT_CUBE = 5062503;

    public static final int NEBILITE_BASE_ID = 3060000;

    public static final int HORNTAIL_NECKLACE[] = {
            1122000, // Horntail Necklace
            1122076, // Chaos Horntail Necklace
            1122151, // Chaos Horntail Necklace (+2)
            1122249, // Dream Horntail Necklace
            1122278, // Mystic Horntail Necklace
    };

    public static final int EXP_2X_COUPON[] = {
            5680484,
            5680342,
            5680275,
            5211122,
            5211121,
            5211120,
            5211049,
            5211048,
            5211047,
            5211046,
            5211000
    };

    public static final short MAX_HAMMER_SLOTS = 2;

    /*
        IDs are found in ItemOption xml. They seem to have different levels but are meant to have only a certain
        level(?). There seem to be entries for every soul stat where levels < 0 are 0 stats and 10+ have the correct
        value and the default level is 10.
     */

    private enum SoulPots {
        STR_3((short) 101),
        DEX_3((short) 102),
        INT_3((short) 103),
        LUK_3((short) 104),
        STR_5((short) 105),
        DEX_5((short) 106),
        INT_5((short) 107),
        LUK_5((short) 108),
        STR_7((short) 109),
        DEX_7((short) 110),
        INT_7((short) 111),
        LUK_7((short) 112),
        STR_10((short) 113),
        DEX_10((short) 114),
        INT_10((short) 115),
        LUK_10((short) 116),
        STR_15((short) 117),
        DEX_15((short) 118),
        INT_15((short) 119),
        LUK_15((short) 120),
        STR_20((short) 121),
        DEX_20((short) 122),
        INT_20((short) 123),
        LUK_20((short) 124),
        MHP_150((short) 131),
        MMP_150((short) 132),
        MHP_200((short) 133),
        MMP_200((short) 134),
        MHP_300((short) 135),
        MMP_300((short) 136),
        MHP_400((short) 137),
        MHP_600((short) 138),
        MHP_800((short) 139),
        MHP_1000((short) 140),
        MHP_1200((short) 141),
        MHP_1500((short) 142),
        ATT_3((short) 151),
        MATT_3((short) 152),
        ATT_4((short) 153),
        MATT_4((short) 154),
        ATT_5((short) 155),
        MATT_5((short) 156),
        ATT_7((short) 159),
        MATT_7((short) 160),
        ATT_10((short) 161),
        MATT_10((short) 162),
        STR_12((short) 163),
        DEX_12((short) 164),
        INT_12((short) 165),
        LUK_12((short) 166),
        STR_24((short) 167),
        DEX_24((short) 168),
        INT_24((short) 169),
        LUK_24((short) 170),
        ATT_6((short) 171),
        MATT_6((short) 172),
        MHP_500((short) 175),
        MHP_1100((short) 176),
        MHP_960((short) 177),
        MHP_2000((short) 178),
        STR_4((short) 179),
        DEX_4((short) 180),
        INT_4((short) 181),
        LUK_4((short) 182),
        MHP_180((short) 183),
        MMP_180((short) 184),
        STR_18((short) 185),
        DEX_18((short) 186),
        INT_18((short) 187),
        LUK_18((short) 188),
        MHP_700((short) 189),
        MHP_1300((short) 190),
        ATT_8((short) 191),
        MATT_8((short) 192),
        ALLSTAT_2((short) 201),
        ALLSTAT_3((short) 202),
        ALLSTAT_5((short) 203),
        ALLSTAT_7((short) 204),
        ALLSTAT_10((short) 205),
        ALLSTAT_12((short) 206),
        ALLSTAT_15((short) 208),
        ALLSTAT_20((short) 209),
        ALLSTAT_8((short) 210),
        ALLSTAT_17((short) 212),
        CRIT_5((short) 301),
        CRIT_7((short) 302),
        CRIT_10((short) 303),
        CRIT_6((short) 304),
        CRIT_12((short) 305),
        CRIT_8((short) 308),
        ATTR_3((short) 306),
        MATTR_3((short) 307),
        ALLSTATR_5((short) 391),
        IED_3((short) 401),
        IED_4((short) 402),
        IED_5((short) 403),
        IED_7((short) 404),
        BD_3((short) 601),
        BD_4((short) 602),
        BD_5((short) 603),
        BD_7((short) 604),
        SKILL_LVL_1((short) 701),
        SKILL_LVL_2((short) 702);

        private short value;

        SoulPots(short value) {
            this.value = value;
        }

        public short getValue() {
            return value;
        }
    }

    private enum MagnificentSoulOptions {
        TIER_1(new Short[] { // Tier 1
            SoulPots.ATT_5.getValue(),
            SoulPots.MATT_5.getValue(),
            SoulPots.ALLSTAT_10.getValue(),
            SoulPots.MHP_1000.getValue(),
            SoulPots.CRIT_5.getValue(),
            SoulPots.IED_3.getValue(),
            SoulPots.BD_3.getValue(),
            SoulPots.SKILL_LVL_1.getValue()
        }),
        TIER_2(new Short[] { // Tier 2
                SoulPots.ATT_6.getValue(),
                SoulPots.MATT_6.getValue(),
                SoulPots.ALLSTAT_12.getValue(),
                SoulPots.MHP_1100.getValue(),
                SoulPots.CRIT_6.getValue(),
                SoulPots.IED_3.getValue(),
                SoulPots.BD_3.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        TIER_3(new Short[] { // Tier 3
                SoulPots.ATT_7.getValue(),
                SoulPots.MATT_7.getValue(),
                SoulPots.ALLSTAT_15.getValue(),
                SoulPots.MHP_1200.getValue(),
                SoulPots.CRIT_7.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        TIER_4(new Short[] { // Tier 4
                SoulPots.ATT_8.getValue(),
                SoulPots.MATT_8.getValue(),
                SoulPots.ALLSTAT_17.getValue(),
                SoulPots.MHP_1300.getValue(),
                SoulPots.CRIT_8.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        TIER_5(new Short[] { // Tier 5
                SoulPots.ATT_10.getValue(),
                SoulPots.MATT_10.getValue(),
                SoulPots.ALLSTAT_20.getValue(),
                SoulPots.MHP_1500.getValue(),
                SoulPots.CRIT_10.getValue(),
                SoulPots.IED_5.getValue(),
                SoulPots.BD_5.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        TIER_6(new Short[] { // Tier 6
                SoulPots.ATTR_3.getValue(),
                SoulPots.MATTR_3.getValue(),
                SoulPots.ALLSTATR_5.getValue(),
                SoulPots.MHP_2000.getValue(),
                SoulPots.CRIT_12.getValue(),
                SoulPots.IED_7.getValue(),
                SoulPots.BD_7.getValue(),
                SoulPots.SKILL_LVL_2.getValue()
        }),
        BLACK_KNIGHT(new Short[] { // Black Knight
                SoulPots.ATT_10.getValue(),
                SoulPots.MATT_8.getValue(),
                SoulPots.ALLSTAT_17.getValue(),
                SoulPots.MHP_1300.getValue(),
                SoulPots.CRIT_8.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        VICIOUS_HUNTER(new Short[] { // Vicious Hunter
                SoulPots.ATT_8.getValue(),
                SoulPots.MATT_8.getValue(),
                SoulPots.ALLSTAT_17.getValue(),
                SoulPots.MHP_1500.getValue(),
                SoulPots.CRIT_8.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        MAD_MAGE(new Short[] { // Mad Mage
                SoulPots.ATT_8.getValue(),
                SoulPots.MATT_10.getValue(),
                SoulPots.ALLSTAT_17.getValue(),
                SoulPots.MHP_1300.getValue(),
                SoulPots.CRIT_8.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        RAMPANT_CYBORG(new Short[] { // Rampant Cyborg
                SoulPots.ATT_8.getValue(),
                SoulPots.MATT_8.getValue(),
                SoulPots.ALLSTAT_17.getValue(),
                SoulPots.MHP_1300.getValue(),
                SoulPots.CRIT_10.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        }),
        BAD_BRAWLER(new Short[] { // Bad Brawler
                SoulPots.ATT_8.getValue(),
                SoulPots.MATT_8.getValue(),
                SoulPots.ALLSTAT_20.getValue(),
                SoulPots.MHP_1300.getValue(),
                SoulPots.CRIT_8.getValue(),
                SoulPots.IED_4.getValue(),
                SoulPots.BD_4.getValue(),
                SoulPots.SKILL_LVL_1.getValue()
        });

        Short[] options;

        MagnificentSoulOptions(Short[] options) {
            this.options = options;
        }

        public Short getRandomOption() {
            return Util.getRandomFromCollection(options);
        }
    }

    private static final int TUC_IGNORE_ITEMS[] = {
            1113231, // Master Ring SS
            1114301, // Reboot Vengeful Ring
            1114302, // Synergy Ring
            1114303, // Cosmos Ring
            1114304, // Reboot Cosmos Ring
            1114305, // Chaos Ring
    };

    public static final int NON_KMS_BOSS_SETS[] = {
        127, // Amaterasu
        128, // Oyamatsumi
        129, // Ame-no-Uzume
        130, // Tsukuyomi
        131, // Susano-o
        315, // Cracked Gollux
        316, // Solid Gollux
        317, // Reinforced Gollux
        318, // Superior Gollux
        328, // Sweetwater
    };

    public static final int NON_KMS_BOSS_ITEMS[] = {
        1032224, // Sweetwater Earrings
        1022211, // Sweetwater Monocle
        1012438, // Sweetwater Tattoo
        1152160, // Sweetwater Shoulder
        1132247, // Sweetwater Belt
        1122269, // Sweetwater Pendant
    };

    // Spell tracing
    private static final int BASE_ST_COST = 30;
    private static final int INNOCENCE_ST_COST = 1337;
    private static final int CLEAN_SLATE_ST_COST = 200;

    // Flames
    public static final double WEAPON_FLAME_MULTIPLIER[] = { 1.0, 2.2, 3.65, 5.35, 7.3, 8.8, 10.25 };
    public static final double WEAPON_FLAME_MULTIPLIER_BOSS_WEAPON[] = { 1.0, 1.0, 3.0, 4.4, 6.05, 8.0, 10.25 }; // Boss weapons do not ever roll stat level 1/2.
    public static final short EQUIP_FLAME_LEVEL_DIVIDER = 40;
    public static final short EQUIP_FLAME_LEVEL_DIVIDER_EXTENDED = 20;

    public static final int EXCEPTIONAL_EX_ALLOWED[] = {
            1152155, // Scarlet Shoulder
            1113015, // Secret Ring
    };

    // Self-made drops per mob
    public static final Map<Integer, Set<DropInfo>> consumableDropsPerLevel = new HashMap<>();
    public static final Map<ItemJob, Map<Integer, Set<DropInfo>>> equipDropsPerLevel = new HashMap<>();

    static {
        initConsumableDrops();
        initEquipDrops();
    }

    private static void initConsumableDrops() {
        consumableDropsPerLevel.put(0, Util.makeSet(
                new DropInfo(2000046, 200), // Red Potion
                new DropInfo(2000014, 200)  // Blue Potion
        ));
        consumableDropsPerLevel.put(20, Util.makeSet(
                new DropInfo(2000002, 200), // White Potion
                new DropInfo(2000006, 200)  // Mana Elixir
        ));
        consumableDropsPerLevel.put(40, Util.makeSet(
                new DropInfo(2001527, 200), // Unagi
                new DropInfo(2022000, 200)  // Pure Water
        ));
        consumableDropsPerLevel.put(60, Util.makeSet(
                new DropInfo(2001527, 200), // Unagi
                new DropInfo(2022000, 200)  // Pure Water
        ));
        consumableDropsPerLevel.put(80, Util.makeSet(
                new DropInfo(2001001, 200), // Ice Cream Pop
                new DropInfo(2001002, 200)  // Pure Water
        ));
        consumableDropsPerLevel.put(100, Util.makeSet(
                new DropInfo(2020012, 100), // Melting Cheese
                new DropInfo(2020013, 100), // Reindeer Milk
                new DropInfo(2020014, 100), // Sunrise Dew
                new DropInfo(2020015, 100), // Sunset Dew
                new DropInfo(2050004, 10)   // All Cure
        ));
    }

    private static void initEquipDrops() {
        List<EquipDrop> drops = (List<EquipDrop>) DatabaseManager.getObjListFromDB(EquipDrop.class);
        for (EquipDrop drop : drops) {
            ItemJob job = drop.getJob();
            int level = drop.getLevel();
            if (!equipDropsPerLevel.containsKey(job)) {
                equipDropsPerLevel.put(job, new HashMap<>());
            }
            Map<Integer, Set<DropInfo>> jobMap = equipDropsPerLevel.get(job);
            if (!jobMap.containsKey(level)) {
                jobMap.put(level, new HashSet<>());
            }
            Set<DropInfo> set = jobMap.get(level);
            set.add(new DropInfo(drop.getId(), 100));
        }
    }

    public static int getGenderFromId(int nItemID) {
        int result;

        if (nItemID / 1000000 != 1 && getItemPrefix(nItemID) != 254 || getItemPrefix(nItemID) == 119 || getItemPrefix(nItemID) == 168)
            return 2;
        switch (nItemID / 1000 % 10) {
            case 0:
                result = 0;
                break;
            case 1:
                result = 1;
                break;
            default:
                return 2;
        }
        return result;
    }

    public static int getBodyPartFromItem(int nItemID, int gender) {
        List<Integer> arr = getBodyPartArrayFromItem(nItemID, gender);
        int result = arr.size() > 0 ? arr.get(0) : 0;
        return result;
    }

    public static List<Integer> getBodyPartArrayFromItem(int itemID, int genderArg) {
        int gender = getGenderFromId(itemID);
        EquipPrefix prefix = EquipPrefix.getByVal(getItemPrefix(itemID));
        List<Integer> bodyPartList = new ArrayList<>();
        if (prefix != EquipPrefix.Emblem && prefix != EquipPrefix.Bit &&
                gender != 2 && genderArg != 2 && gender != genderArg) {
            return bodyPartList;
        }
        if(prefix != null) {
            switch (prefix) {
                case Hat:
                    bodyPartList.add(BodyPart.Hat.getVal());
                    bodyPartList.add(BodyPart.EvanHat.getVal());
                    bodyPartList.add(BodyPart.APHat.getVal());
                    bodyPartList.add(BodyPart.DUHat.getVal());
                    bodyPartList.add(BodyPart.ZeroHat.getVal());
                    break;
                case FaceAccessory:
                    bodyPartList.add(BodyPart.FaceAccessory.getVal());
                    bodyPartList.add(BodyPart.APFaceAccessory.getVal());
                    bodyPartList.add(BodyPart.DUFaceAccessory.getVal());
                    bodyPartList.add(BodyPart.ZeroFaceAccessory.getVal());
                    break;
                case EyeAccessory:
                    bodyPartList.add(BodyPart.EyeAccessory.getVal());
                    bodyPartList.add(BodyPart.ZeroEyeAccessory.getVal());
                    break;
                case Earrings:
                    bodyPartList.add(BodyPart.Earrings.getVal());
                    bodyPartList.add(BodyPart.ZeroEarrings.getVal());
                    break;
                case Top:
                case Overall:
                    bodyPartList.add(BodyPart.Top.getVal());
                    bodyPartList.add(BodyPart.APTop.getVal());
                    bodyPartList.add(BodyPart.DUTop.getVal());
                    bodyPartList.add(BodyPart.ZeroTop.getVal());
                    break;
                case Bottom:
                    bodyPartList.add(BodyPart.Bottom.getVal());
                    bodyPartList.add(BodyPart.APBottom.getVal());
                    bodyPartList.add(BodyPart.ZeroBottom.getVal());
                    break;
                case Shoes:
                    bodyPartList.add(BodyPart.Shoes.getVal());
                    bodyPartList.add(BodyPart.APShoes.getVal());
                    bodyPartList.add(BodyPart.ZeroShoes.getVal());
                    break;
                case Gloves:
                    bodyPartList.add(BodyPart.Gloves.getVal());
                    bodyPartList.add(BodyPart.APGloves.getVal());
                    bodyPartList.add(BodyPart.DUGloves.getVal());
                    bodyPartList.add(BodyPart.ZeroGloves.getVal());
                    break;
                case Shield:
                case Katara:
                case SecondaryWeapon:
                case Lapis:
                    bodyPartList.add(BodyPart.Shield.getVal());
                    break;
                case Lazuli:
                    bodyPartList.add(BodyPart.Weapon.getVal());
                    break;
                case Cape:
                    bodyPartList.add(BodyPart.Cape.getVal());
                    bodyPartList.add(BodyPart.APCape.getVal());
                    bodyPartList.add(BodyPart.DUCape.getVal());
                    bodyPartList.add(BodyPart.ZeroCape.getVal());
                    break;
                case Ring:
                    bodyPartList.add(BodyPart.Ring1.getVal());
                    bodyPartList.add(BodyPart.Ring2.getVal());
                    bodyPartList.add(BodyPart.Ring3.getVal());
                    bodyPartList.add(BodyPart.Ring4.getVal());
                    bodyPartList.add(BodyPart.ZeroRing1.getVal());
                    bodyPartList.add(BodyPart.ZeroRing2.getVal());
                    break;
                case Pendant:
                    bodyPartList.add(BodyPart.Pendant.getVal());
                    bodyPartList.add(BodyPart.ExtendedPendant.getVal());
                    break;
                case Belt:
                    bodyPartList.add(BodyPart.Belt.getVal());
                    break;
                case Medal:
                    bodyPartList.add(BodyPart.Medal.getVal());
                    break;
                case Shoulder:
                    bodyPartList.add(BodyPart.Shoulder.getVal());
                    break;
                case PocketItem:
                    bodyPartList.add(BodyPart.PocketItem.getVal());
                    break;
                case MonsterBook:
                    bodyPartList.add(BodyPart.MonsterBook.getVal());
                    break;
                case Badge:
                    bodyPartList.add(BodyPart.Badge.getVal());
                    break;
                case Emblem:
                    bodyPartList.add(BodyPart.Emblem.getVal());
                    break;
                case Totem:
                    bodyPartList.add(BodyPart.Totem1.getVal());
                    bodyPartList.add(BodyPart.Totem2.getVal());
                    bodyPartList.add(BodyPart.Totem3.getVal());
                    break;
                case MachineEngine:
                    bodyPartList.add(BodyPart.MachineEngine.getVal());
                    break;
                case MachineArm:
                    bodyPartList.add(BodyPart.MachineArm.getVal());
                    break;
                case MachineLeg:
                    bodyPartList.add(BodyPart.MachineLeg.getVal());
                    break;
                case MachineFrame:
                    bodyPartList.add(BodyPart.MachineFrame.getVal());
                    break;
                case MachineTransistor:
                    bodyPartList.add(BodyPart.MachineTransistor.getVal());
                    break;
                case Android:
                    bodyPartList.add(BodyPart.Android.getVal());
                    break;
                case MechanicalHeart:
                    bodyPartList.add(BodyPart.MechanicalHeart.getVal());
                    break;
                case Bit:
                    for (int id = BodyPart.BitsBase.getVal(); id <= BodyPart.BitsEnd.getVal(); id++) {
                        bodyPartList.add(id);
                    }
                    break;
                case PetWear:
                    bodyPartList.add(BodyPart.PetWear1.getVal());
                    bodyPartList.add(BodyPart.PetWear2.getVal());
                    bodyPartList.add(BodyPart.PetWear3.getVal());
                    break;
                // case 184: // unknown, equip names are untranslated and google search results in hekaton screenshots
                // case 185:
                // case 186:
                // case 187:
                // case 188:
                // case 189:
                case TamingMob:
                    bodyPartList.add(BodyPart.TamingMob.getVal());
                    break;
                case Saddle:
                    bodyPartList.add(BodyPart.Saddle.getVal());
                    break;
                case EvanHat:
                    bodyPartList.add(BodyPart.EvanHat.getVal());
                    break;
                case EvanPendant:
                    bodyPartList.add(BodyPart.EvanPendant.getVal());
                    break;
                case EvanWing:
                    bodyPartList.add(BodyPart.EvanWing.getVal());
                    break;
                case EvanShoes:
                    bodyPartList.add(BodyPart.EvanShoes.getVal());
                    break;
                default:
                    if (ItemConstants.isLongOrBigSword(itemID) || ItemConstants.isWeapon(itemID)) {
                        bodyPartList.add(BodyPart.Weapon.getVal());
                        if (ItemConstants.isFan(itemID)) {
                            bodyPartList.add(BodyPart.HakuFan.getVal());
                        } else {
                            bodyPartList.add(BodyPart.ZeroWeapon.getVal());
                        }
                    } else {
                        log.debug("Unknown type? id = " + itemID);
                    }
                    break;
            }
        }
        else
        {
            log.debug("Unknown type? id = " + itemID);
        }
        return bodyPartList;

    }

    private static int getItemPrefix(int nItemID) {
        return nItemID / 10000;
    }

    public static boolean isLongOrBigSword(int nItemID) {
        return getItemPrefix(nItemID) == EquipPrefix.Lapis.getVal() || getItemPrefix(nItemID) == EquipPrefix.Lazuli.getVal();
    }

    private static boolean isFan(int nItemID) {
        return getItemPrefix(nItemID) == EquipPrefix.Fan.getVal();
    }

    public static int getWeaponType(int itemID) {
        if (itemID / 1000000 != 1) {
            return 0;
        }
        return getItemPrefix(itemID) % 100;
    }

    public static boolean isThrowingItem(int itemID) {
        return isThrowingStar(itemID) || isBullet(itemID) || isBowArrow(itemID);
    }

    public static boolean isThrowingStar(int itemID) {
        return getItemPrefix(itemID) == 207;
    }

    public static boolean isBullet(int itemID) {
        return getItemPrefix(itemID) == 233;
    }

    public static boolean isBowArrow(int itemID) {
        return itemID / 1000 == 2060;
    }

    public static boolean isFamiliar(int itemID) {
        return getItemPrefix(itemID) == 287;
    }

    public static boolean isEnhancementScroll(int scrollID) {
        return scrollID / 100 == 20493;
    }

    public static boolean isHat(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Hat.getVal();
    }

    public static boolean isWeapon(int itemID) {
        return itemID >= 1210000 && itemID < 1600000;
    }

    public static boolean isSecondary(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.SecondaryWeapon.getVal();
    }

    public static boolean isShield(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Shield.getVal();
    }

    public static boolean isAccessory(int itemID) {
        return (itemID >= 1010000 && itemID < 1040000) || (itemID >= 1122000 && itemID < 1153000) ||
                (itemID >= 1112000 && itemID < 1113000) || (itemID >= 1670000 && itemID < 1680000);
    }

    public static boolean isFaceAccessory(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.FaceAccessory.getVal();
    }

    public static boolean isEyeAccessory(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.EyeAccessory.getVal();
    }

    public static boolean isEarrings(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Earrings.getVal();
    }

    public static boolean isTop(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Top.getVal();
    }

    public static boolean isOverall(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Overall.getVal();
    }

    public static boolean isBottom(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Bottom.getVal();
    }

    public static boolean isShoe(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Shoes.getVal();
    }

    public static boolean isGlove(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Gloves.getVal();
    }

    public static boolean isCape(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Cape.getVal();
    }

    public static boolean isArmor(int itemID) {
        return !isAccessory(itemID) && !isWeapon(itemID);
    }

    public static boolean isRing(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Ring.getVal();
    }

    public static boolean isPendant(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Pendant.getVal();
    }

    public static boolean isBelt(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Belt.getVal();
    }

    public static boolean isMedal(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Medal.getVal();
    }

    public static boolean isShoulder(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Shoulder.getVal();
    }

    public static boolean isPocketItem(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.PocketItem.getVal();
    }

    public static boolean isMonsterBook(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.MonsterBook.getVal();
    }

    public static boolean isBadge(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Badge.getVal();
    }

    public static boolean isEmblem(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Emblem.getVal();
    }

    public static boolean isTotem(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Totem.getVal();
    }

    public static boolean isAndroid(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.Android.getVal();
    }

    public static boolean isMechanicalHeart(int itemID) {
        return getItemPrefix(itemID) == EquipPrefix.MechanicalHeart.getVal();
    }

    public static boolean isRebirthFlame(int itemId) { return itemId >= 2048700 && itemId < 2048800; }

    public static boolean isNebulite(int itemId) { return getItemPrefix(itemId) == 306; }

    public static boolean canEquipTypeHavePotential(int itemid) {
        return isRing(itemid) ||
                isPendant(itemid) ||
                isWeapon(itemid) ||
                isBelt(itemid) ||
                isHat(itemid) ||
                isFaceAccessory(itemid) ||
                isEyeAccessory(itemid) ||
                isOverall(itemid) ||
                isTop(itemid) ||
                isBottom(itemid) ||
                isShoe(itemid) ||
                isEarrings(itemid) ||
                isShoulder(itemid) ||
                isGlove(itemid) ||
                isEmblem(itemid) ||
                isBadge(itemid) ||
                isShield(itemid) ||
                isCape(itemid) ||
                isMechanicalHeart(itemid);
    }

    public static boolean canEquipHavePotential(Equip equip) {
        return !equip.isCash() &&
                canEquipTypeHavePotential(equip.getItemId()) &&
                !equip.isNoPotential() &&
                (ItemData.getEquipById(equip.getItemId()).getTuc() >= 1 || isTucIgnoreItem(equip.getItemId()));
    }

    public static boolean canEquipHaveFlame(Equip equip) {
        return !equip.isCash() && (isPendant(equip.getItemId()) ||
                (isWeapon(equip.getItemId()) && !isSecondary((equip.getItemId())) && !isShield((equip.getItemId()))) ||
                isBelt(equip.getItemId()) ||
                isHat(equip.getItemId()) ||
                isFaceAccessory(equip.getItemId()) ||
                isEyeAccessory(equip.getItemId()) ||
                isOverall(equip.getItemId()) ||
                isTop(equip.getItemId()) ||
                isBottom(equip.getItemId()) ||
                isShoe(equip.getItemId()) ||
                isEarrings(equip.getItemId()) ||
                Arrays.asList(EXCEPTIONAL_EX_ALLOWED).contains(equip.getItemId()) ||
                isGlove(equip.getItemId()) ||
                isCape(equip.getItemId()) ||
                isPocketItem(equip.getItemId()));
    }

    public static boolean canEquipGoldHammer(Equip equip) {
        Equip defaultEquip = ItemData.getEquipById(equip.getItemId());
        return !(Arrays.asList(HORNTAIL_NECKLACE).contains(equip.getItemId()) ||
                equip.getIuc() >= defaultEquip.getIUCMax() ||
                defaultEquip.getTuc() <= 0); // No upgrade slots by default
    }

    public static boolean isGoldHammer(Item item) {
        return getItemPrefix(item.getItemId()) == 247;
    }

    /**
     * Gets potential tier for a line.
     * Accounts prime lines too.
     * @param line Potential line.
     * @param grade Our current potential grade.
     */
    public static ItemGrade getLineTier(int line, ItemGrade grade) {
        if (line == 0 || Util.succeedProp(PRIME_LINE_CHANCE)) {
            return grade;
        }
        return ItemGrade.getOneTierLower(grade.getVal());
    }

    /**
     * Determines whether a nebulite can be mounted on an equip.
     * @param equip Equip item.
     * @param nebulite The nebulite to mount on the equip.
     */
    public static boolean nebuliteFitsEquip(Equip equip, Item nebulite) {
        int nebuliteId = nebulite.getItemId();
        Map<ScrollStat, Integer> vals = ItemData.getItemInfoByID(nebuliteId).getScrollStats();
        if (vals.size() == 0) {
            return false;
        }
        ItemOptionType type = ItemOptionType.getByVal(vals.getOrDefault(ScrollStat.optionType, 0));
        int equipId = equip.getItemId();
        switch(type) {
            case AnyEquip:
                return true;
            case Weapon: // no emblems for nebs here
                return isWeapon(equipId) || isShield(equipId);
            case AnyExceptWeapon:
                return !isWeapon(equipId) && !isShield(equipId);
            case ArmorExceptGlove:
                return isBelt(equipId) || isHat(equipId) || isOverall(equipId) || isTop(equipId) || isBottom(equipId) || isShoe(equipId) || isCape(equipId);
            case Accessory:
                return isRing(equipId) || isPendant(equipId) || isFaceAccessory(equipId) || isEyeAccessory(equipId) || isEarrings(equipId) || isShoulder(equipId);
            case Hat:
                return isHat(equipId);
            case Top:
                return isTop(equipId) || isOverall(equipId);
            case Bottom:
                return isBottom(equipId) || isOverall(equipId);
            case Glove:
                return isGlove(equipId);
            case Shoes:
                return isShoe(equipId);
            default:
                return false;
        }
    }

    public static List<ItemOption> getOptionsByEquip(Equip equip, boolean bonus, int line) {
        int id = equip.getItemId();
        Collection<ItemOption> data = ItemData.getItemOptions().values();
        ItemGrade grade = getLineTier(line, ItemGrade.getGradeByVal(bonus ? equip.getBonusGrade() : equip.getBaseGrade()));

        // need a list, as we take a random item from it later on
        List<ItemOption> res = data.stream().filter(
                io -> io.getOptionType() == ItemOptionType.AnyEquip.getVal() &&
                io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                .collect(Collectors.toList());
        if (isWeapon(id) || isShield(id) || isEmblem(id)) {
            // TODO: block boss% on emblem
            res.addAll(data.stream().filter(
                    io -> io.getOptionType() == ItemOptionType.Weapon.getVal()
                    &&  io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus
            ).collect(Collectors.toList()));
        } else {
            res.addAll(data.stream().filter(
                    io -> io.getOptionType() == ItemOptionType.AnyExceptWeapon.getVal()
                    && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                    .collect(Collectors.toList()));
            if (isRing(id) || isPendant(id) || isFaceAccessory(id) || isEyeAccessory(id) || isEarrings(id)) {
                res.addAll(data.stream().filter(
                        io -> io.getOptionType() == ItemOptionType.Accessory.getVal()
                        && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                        .collect(Collectors.toList()));
            } else {
                if (isHat(id)) {
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.Hat.getVal()
                            && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                }
                if (isTop(id) || isOverall(id)) {
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.Top.getVal()
                            && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                }
                if (isBottom(id) || isOverall(id)) {
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.Bottom.getVal()
                            && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                }
                if (isGlove(id)) {
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.Glove.getVal()
                            && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                } else {
                    // gloves are not counted for this one
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.ArmorExceptGlove.getVal()
                                    && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                }
                if (isShoe(id)) {
                    res.addAll(data.stream().filter(
                            io -> io.getOptionType() == ItemOptionType.Shoes.getVal()
                            && io.hasMatchingGrade(grade.getVal()) && io.isBonus() == bonus)
                            .collect(Collectors.toList()));
                }
            }
        }
        return res.stream().filter(io -> io.getReqLevel() <= equip.getrLevel() + equip.getiIncReq()).collect(Collectors.toList());
    }

    public static List<Integer> getWeightedOptionsByEquip(Equip equip, boolean bonus, int line) {
        List<Integer> res = new ArrayList<>();
        List<ItemOption> data = getOptionsByEquip(equip, bonus, line);
        for(ItemOption io : data) {
            for (int i = 0; i < io.getWeight(); i++) {
                res.add(io.getId());
            }
        }
        return res;
    }

    public static int getRandomOption(Equip equip, boolean bonus, int line) {
        List<Integer> data = getWeightedOptionsByEquip(equip, bonus, line);
        return data.get(Util.getRandom(data.size()));
    }

    public static int getTierUpChance(int id) {
        int res = 0;
        switch(id) {
            case ItemConstants.RED_CUBE: // Red cube
            case ItemConstants.BONUS_POT_CUBE: // Bonus potential cube
                res = 30;
                break;
            case ItemConstants.BLACK_CUBE:
            case ItemConstants.WHITE_BONUS_POT_CUBE:
                res = 40;
                break;
        }
        return res;
    }

    public static boolean isEquip(int id) {
        return id / 1000000 == 1;
    }

    public static boolean isClaw(int id) {
        return getItemPrefix(id) == 147;
    }

    public static boolean isBow(int id) {
        return getItemPrefix(id) == 145;
    }

    public static boolean isXBow(int id) {
        return getItemPrefix(id) == 146;
    }

    public static boolean isGun(int id) {
        return getItemPrefix(id) == 149;
    }

    public static boolean isXBowArrow(int id) {
        return id / 1000 == 2061;
    }

    public static InvType getInvTypeByItemID(int itemID) {
        if(isEquip(itemID)) {
            return EQUIP;
        } else {
            ItemInfo ii = ItemData.getItemInfoByID(itemID);
            if(ii == null) {
                return null;
            }
            return ii.getInvType();
        }
    }

    public static Set<Integer> getRechargeablesList() {
        Set<Integer> itemList = new HashSet<>();
        // all throwing stars
        for(int i = 2070000; i <= 2070016; i++) {
            itemList.add(i);
        }
        itemList.add(2070018);
        itemList.add(2070023);
        itemList.add(2070024);
        itemList.add(2070026);
        // all bullets
        for(int i = 2330000; i <= 2330006; i++) {
            itemList.add(i);
        }
        itemList.add(2330008);
        itemList.add(2330016);
        itemList.add(2331000);
        itemList.add(2332000);
        return itemList;
    }

    public static boolean isRechargable(int itemId) {
        return isThrowingStar(itemId) || isBullet(itemId);
    }

    public static int getDamageSkinIDByItemID(int itemID) {
        switch(itemID) {
            case 2431965: // base damage Skin:
                return 0;
            case 2431966: // digital Sunrise Skin Damage:
            case 2432084: // digital Sunrise damage the skin
                return 1;
            case 2431967: // Kritias Skin Damage:
                return 2;
            case 2432131: // Party Quest Skin Damage:
                return 3;
            case 2432153: // Hard Hitting:
            case 2432638: // Creative Impact Damage Skin
            case 2432659: // Creative Impact Damage Skin
                return 4;
            case 2432154: // sweet traditional Han Skin Damage:
            case 2432637: // sweet traditional one and damage the skin
            case 2432658: // sweet traditional one and damage the skin
                return 5;
            case 2432207: // Club Henesys' damage Skin:
                 return 6;
            case 2432354: // Merry Christmas Skin Damage:
                 return 7;
            case 2432355: // Snow Blossom Skin Damage:
            case 2432972: // Snow Blossom Skin Damage
                 return 8;
            case 2432465: // damage the skin of Alicia:
                 return 9;
            case 2432479: // Dorothy skin damage:
                 return 10;
            case 2432526: // Keyboard Warrior Skin Damage:
            case 2432639: // Keyboard Warrior Skin Damage
            case 2432660: // Keyboard Warrior Skin Damage
                 return 11;
            case 2432532: // spring breeze rustling skin damage:
                 return 12;
            case 2432592: // solo troops skin damage:
                 return 13;
            case 2432640: // Reminiscence skin damage:
            case 2432661: // Remy you damage the skin Suns
                 return 14;
            case 2432710: // Orange Mushroom Skin Damage:
                 return 15;
            case 2432836: // crown damage Skin:
                 return 16;
            case 2432973: // monotone skin damage:
                 return 17;
            case 2433063: // Star Planet skin:
                 return 18;
            case 2433178: // Halloween Skin (bones):
                 return 20;
            case 2433456: // Hangul Skin:
                 return 21;
            case 2435960: // Fried Chicken Dmg Skin(Unknown ItemID):
                 return 22;
            case 2433715: // Striped Damage Skin:
                 return 23;
            case 2433804: // Couples Army Damage Skin:
                 return 24;
            case 5680343: // Star Damage Skin:
                 return 25;
            case 2433913: // Yeti and Pepe Damage Skin:
                 return 26;
            case 2433980: // Slime and Mushroom Damage Skin:
                 return 27;
            case 2433981: // Pink bean Damage skin:
                 return 28;
            case 2436229: // Pig Bar Dmg Skin(Unknown ItemID):
                 return 29;
//         case 2432659: // Hard-Hitting Dmg Skin (already in): 30
//         case 2return 432526;: // Keyboard Warrior (already in): 31
//         case 2432710: // Orange mushroom Skin Damage(already in): 32
//         case 2432355: // Snowflake Dmg Skin(already in): 33
            case 2434248: // Rainbow Boom Damage Skin:
                 return 34;
            case 2433362: // Night Sky Damage Skin:
                 return 35;
            case 2434274: // Marshmallow Damage Skin:
                 return 36;
            case 2434289: // Mu Lung Dojo Dmg Skin:
                 return 37;
            case 2434390: // Teddy Damage Skin:
                 return 38;
            case 2434391: // Mighty Ursus Damage Skin:
                 return 39;
            case 5680395: // Scorching Heat Damage Skin:
                 return 40;
            case 2434528: // USA Damage Skin:
                 return 41;
            case 2434529: // Churro Damage Skin:
                 return 42;
            case 2434530: // Singapore Night Damage Skin:
                 return 43;
            case 2433571: // Scribble Crush Damage Skin:
                 return 44;
            case 2434574: // Full Moon Damage Skin:
                 return 45;
            case 2433828: // White Heaven Sun Damage Skin:
                 return 46;
            case 2432804: // Princess No Damage Skin:
                 return 47;
            case 2434654: // Murgoth Damage Skin:
                 return 48;
            case 2435326: // Nine-Tailed Fox Damage Skin:
                 return 49;
            case 2432749: // Zombie Damage Skin:
                 return 50;
            case 2434710: // MVP Special Damage Skin:
                 return 51;
            case 2433777: // Black Heaven Damage Skin:
                 return 52;
            case 2434824: // Monster Park Damage Skin:
                 return 53;
                // case 2431966: // Digital Damage Skin(already in): 54 - (1)
                // case 2431967: // Kritias Damage Skin(already in): 55 - (2)
                // case 2432154: // Sweet tea cake Damage Skin(already in): 56 - (5)
                // case 2432354: // Merry Christmas Damage Skin(already in): 57 - (7)
                // case 2432532: // Gentle spring breeze damage skin(already in): 58 - (12)
                // case 2433715: // Striped Damage Skin(already in): 59 - (23)
                // case 2433063: // Star Damage Skin(already in): 60 - (25)
                // case 2433913: // Yeti and Pepe Damage Skin(already in): 61 - (26)
                // case 2433980: // Slime and Mushroom Damage Skin(already in): 62 - (27)
                // case 2434248: // Rainbow Boom Damage Skin(already in): 63 - (34)
                // case 2433362: // Night Sky Damage Skin(already in): 64 - (35)
                // case 2434274: // Marshmallow Damage Skin(already in): 65 - (36)
                // case 2434390: // Teddy Damage Skin(already in): 66 - (38)
                // case 5680395: // Scorching Heat Damage Skin(already in): 67 - (40)
                // case 2434528: // USA Damage Skin(already in): 68 - (41)
                // case 2434529: // Churro Damage Skin(already in): 69 - (42)
                // case 2434530: // Singapore Night Damage Skin(already in): 70 - (43)
                // case 2433571: // Scribble Crush Damage Skin(already in): 71 - (44)
                // case 2434574: // Full Moon Damage Skin(already in): 72 - (45)
                // case 2433828: // White Heaven Sun Damage Skin(already in): 73 - (46)
            case 2434662: // Jelly Beans Damage Skin:
                return 74;
            case 2434664: // Soft-Serve Damage Skin:
                return 75;
            case 2434868: // Christmas lights Damage skin:
                return 76;
            case 2436041: // Phantom Damage Skin:
                return 77;
            case 2436042: // Mercedes Damage Skin:
                return 78;
            case 2435046: // Fireworks Damage Skin:
                return 79;
            case 2435047: // Heart Balloon Damage Skin:
                return 80;
            case 2435836: // Neon Sign Damage Skin:
                return 81;
            case 2435141: // Freeze Tag Damage Skin:
                return 82;
            case 2435179: // Candy Damage Skin:
                return 83;
            case 2435162: // Antique Gold Damage Skin:
                return 84;
            case 2435157: // Calligraphy Damage Skin:
                return 85;
            case 2435835: // Explosion Damage Skin:
                return 86;
            case 2435159: // Snow-wing Damage Skin:
                return 87;
            case 2436044: // Miho Damage Skin:
                return 88;
            case 2434663: // Donut Damage Skin:
                return 89;
            case 2435182: // Music Score Damage Skin:
                return 90;
            case 2435850: // Moon Bunny Damage Skin:
                return 91;
            case 2435184: // Forest of Tenacity Damage Skin:
                return 92;
            case 2435222: // Festival Tortoise Damage Skin:
                return 93;
            case 2435293: // April Fools' Damage Skin:
                return 94;
            case 2435313: // Blackheart Day Damage Skin:
                return 95;
            case 2435331: // Bubble April Fools' Damage Skin:
                return 96;
            case 2435332: // Retro April Fools' Damage Skin:
                return 97;
            case 2435333: // Monochrome April Fools' Damage Skin:
                return 98;
            case 2435334: // Sparkling April Fools' Damage Skin:
                return 99;
            case 2435316: // Haste Damage Skin:
                return 100;
            case 2435408: // 13th Anniversary Maple Leaf Damage Skin:
                return 101;
            case 2435427: // Cyber Damage Skin:
                return 102;
            case 2435428: // Cosmic Damage Skin:
                return 103;
            case 2435429: // Choco Donut Damage Skin:
                return 104;
            case 2435456: // Lovely Damage Skin:
                return 105;
            case 2435493: // Monster Balloon Damage Skin:
                return 106;
                // case 2435331: // Bubble April Fools' Damage Skin(already in): 107 - (96)
                // case 2435334: // Sparkling April Fools' Damage Skin(already in): 108 - (99)
            case 2435959: // Henesys Damage Skin (unknown ID):
                return 109;
            case 2435958: // Leafre Damage Skin (unknown ID):
                return 110;
            case 2435431: // Algebraic Damage Skin:
                return 111;
            case 2435430: // Blue Fire Damage Skin:
                return 112;
            case 2435432: // Purple Damage Skin:
                return 113;
            case 2435433: // Nanopixel Damage Skin:
                return 114;
            case 2434601: // Invisible Damage Skin(unknown ID):
                return 115;
            case 2435521: // Crystal Damage Skin:
                return 116;
            case 2435196: // Crow Damage Skin:
                return 117;
            case 2435523: // Chocolate Damage Skin:
                return 118;
            case 2435524: // Spark Damage Skin:
                return 119;
            case 2435538: // Royal Damage Skin:
                return 120;
            case 2435832: // Chrome Damage Skin (Ver.1):
                return 121;
            case 2435833: // Neon Lights Damage Skin:
                return 122;
            case 2435839: // Cosmic Damage Skin(Cards):
                return 123;
            case 2435840: // Gilded Damage Skin:
                return 124;
            case 2435841: // Batty Damage Skin:
                return 125;
            case 2435849: // Monochrome April Fools' Damage Skin:
                return 126;
            case 2435972: // Vanishing Journey Damage Skin:
                return 127;
            case 2436023: // Chu Chu Damage Skin:
                return 128;
            case 2436024: // Lachelein Damage Skin:
                return 129;
            case 2436026: // Poison flame Damage Skin:
                return 130;
            case 2436027: // Blue Strike Damage Skin:
                return 131;
            case 2436028: // Music Power Damage Skin:
                return 132;
            case 2436029: // Collage Power Damage Skin:
                return 133;
            case 2436045: // Starlight Aurora Damage Skin:
                return 134;
            default:
                return 0;
        }
    }

    public static boolean isMasteryBook(int itemId) {
        return getItemPrefix(itemId) == 229;
    }

    public static boolean isPet(int itemId) {
        return getItemPrefix(itemId) == 500;
    }

    public static boolean isSoulEnchanter(int itemID) {
        return itemID / 1000 == 2590;
    }

    public static boolean isSoul(int itemID) {
        return itemID / 1000 == 2591;
    }

    public static short getSoulOptionFromSoul(int itemId) {
        short id = 0;
        switch(itemId) {
            // Beefy
            case 2591096: // Ani
            case 2591089: // Spirit of Rock
            case 2591265: // Black Slime
            case 2591164: // Xerxes
            case 2591017: // Ani untradeable
            case 2591010: // Spirit of Rock untradeable
            case 2591249: // Black slime untradeable
                id = SoulPots.STR_3.getValue();
                break;
            case 2591187: // Ephenia untradeable
            case 2591203: // Ephenia
                id = SoulPots.STR_4.getValue();
                break;
            case 2591103: // Dragon Rider
            case 2591031: // Rex untradeable
            case 2591024: // Dragon Rider untradeable
            case 2591110: // Rex
                id = SoulPots.STR_5.getValue();
                break;
            case 2591218: // Pianus untradeable
            case 2591117: // Mu Gong
            case 2591234: // Pianus
            case 2591038: // Mu Gong untradeable
                id = SoulPots.STR_7.getValue();
                break;
            case 2591124: // Balrog
            case 2591045: // Balrog untradeable
                id = SoulPots.STR_10.getValue();
                break;
            case 2591155: // Zakum untradeable
            case 2591468: // Gold Dragon
            case 2591476: // Red Tiger
            case 2591171: // Zakum
                id = SoulPots.STR_12.getValue();
                break;
            case 2591241: // Hilla
            case 2591132: // Von Leon
            case 2591225: // Hilla untradeable
            case 2591065: // Von Leon untradeable
                id = SoulPots.STR_15.getValue();
                break;
            case 2591210: // Arkarium
            case 2591306: // Mad Mage (Elite) untradeable
            case 2591324: // Vicious Hunter (Elite) untradeable
            case 2591315: // Rampant Cyborg (Elite) untradeable
            case 2591333: // Bad Brawler (Elite) untradeable
            case 2591194: // Arkarium untradeable
            case 2591350: // Mad Mage (Elite)
            case 2591366: // Vicious Hunter (Elite)
            case 2591358: // Rampant Cyborg (Elite)
            case 2591374: // Bad Brawler (Elite)
                id = SoulPots.STR_18.getValue();
                break;
            case 2591428: // Pierre
            case 2591392: // Von Bon untradeable
            case 2591509: // Ursus
            case 2591518: // Ursus untradeable
            case 2591140: // Pink Bean
            case 2591055: // Pink Bean untradeable
            case 2591297: // Black Knight (Elite) untradeable
            case 2591342: // Black Knight (Elite)
            case 2591383: // Pierre untradeable
            case 2591436: // Von Bon
                id = SoulPots.STR_20.getValue();
                break;
            case 2591179: // Cygnus
            case 2591075: // Cygnus untradeable
            case 2591272: // Magnus
            case 2591256: // Magnus untradeable
            case 2591288: // Murgoth
            case 2591401: // Crimson Queen untradeable
            case 2591444: // Crimson Queen
            case 2591410: // Vellum untradeable
            case 2591452: // Vellum
            case 2591419: // Lotus untradeable
            case 2591460: // Lotus
            case 2591564: // Damien untradeable
            case 2591573: // Damien
            case 2591582: // Lucid
                id = SoulPots.STR_24.getValue();
                break;

            // Swift
            case 2591097: // Ani
            case 2591090: // Spirit of Rock
            case 2591266: // Black Slime
            case 2591165: // Xerxes
            case 2591018: // Ani untradeable
            case 2591011: // Spirit of Rock untradeable
            case 2591250: // Black slime untradeable
                id = SoulPots.DEX_3.getValue();
                break;
            case 2591188: // Ephenia untradeable
            case 2591204: // Ephenia
                id = SoulPots.DEX_4.getValue();
                break;
            case 2591104: // Dragon Rider
            case 2591032: // Rex untradeable
            case 2591025: // Dragon Rider untradeable
            case 2591111: // Rex
                id = SoulPots.DEX_5.getValue();
                break;
            case 2591219: // Pianus untradeable
            case 2591118: // Mu Gong
            case 2591235: // Pianus
            case 2591039: // Mu Gong untradeable
                id = SoulPots.DEX_7.getValue();
                break;
            case 2591125: // Balrog
            case 2591046: // Balrog untradeable
                id = SoulPots.DEX_10.getValue();
                break;
            case 2591156: // Zakum
            case 2591469: // Gold Dragon
            case 2591477: // Red Tiger
            case 2591172: // Zakum
                id = SoulPots.DEX_12.getValue();
                break;
            case 2591242: // Hilla
            case 2591133: // Von Leon
            case 2591226: // Hilla untradeable
            case 2591066: // Von Leon untradeable
                id = SoulPots.DEX_15.getValue();
                break;
            case 2591211: // Arkarium
            case 2591298: // Black Knight (Elite) untradeable
            case 2591307: // Mad Mage (Elite) untradeable
            case 2591316: // Rampant Cyborg (Elite) untradeable
            case 2591334: // Bad Brawler (Elite) untradeable
            case 2591195: // Arkarium untradeable
            case 2591343: // Black Knight (Elite)
            case 2591351: // Mad Mage (Elite)
            case 2591359: // Rampant Cyborg (Elite)
            case 2591375: // Bad Brawler (Elite)
                id = SoulPots.DEX_18.getValue();
                break;
            case 2591429: // Pierre
            case 2591384: // Pierre untradeable
            case 2591393: // Von Bon untradeable
            case 2591437: // Von Bon
            case 2591510: // Ursus
            case 2591519: // Ursus untradeable
            case 2591141: // Pink Bean
            case 2591056: // Pink Bean untradeable
            case 2591325: // Vicious Hunter (Elite) untradeable
            case 2591367: // Vicious Hunter (Elite)
                id = SoulPots.DEX_20.getValue();
                break;
            case 2591180: // Cygnus
            case 2591076: // Cygnus untradeable
            case 2591273: // Magnus
            case 2591257: // Magnus untradeable
            case 2591289: // Murgoth
            case 2591402: // Crimson Queen untradeable
            case 2591445: // Crimson Queen
            case 2591411: // Vellum untradeable
            case 2591453: // Vellum
            case 2591420: // Lotus untradeable
            case 2591461: // Lotus
            case 2591565: // Damien untradeable
            case 2591574: // Damien
            case 2591583: // Lucid
                id = SoulPots.DEX_24.getValue();
                break;

            // Clever
            case 2591098: // Ani
            case 2591091: // Spirit of Rock
            case 2591267: // Black Slime
            case 2591166: // Xerxes
            case 2591019: // Ani untradeable
            case 2591012: // Spirit of Rock untradeable
            case 2591251: // Black slime untradeable
                id = SoulPots.INT_3.getValue();
                break;
            case 2591189: // Ephenia untradeable
            case 2591205: // Ephenia
                id = SoulPots.INT_4.getValue();
                break;
            case 2591105: // Dragon Rider
            case 2591033: // Rex untradeable
            case 2591026: // Dragon Rider untradeable
            case 2591112: // Rex
                id = SoulPots.INT_5.getValue();
                break;
            case 2591220: // Pianus untradeable
            case 2591119: // Mu Gong
            case 2591236: // Pianus
            case 2591040: // Mu Gong untradeable
                id = SoulPots.INT_7.getValue();
                break;
            case 2591126: // Balrog
            case 2591047: // Balrog untradeable
                id = SoulPots.INT_10.getValue();
                break;
            case 2591157: // Zakum
            case 2591470: // Gold Dragon
            case 2591478: // Red Tiger
            case 2591173: // Zakum
                id = SoulPots.INT_12.getValue();
                break;
            case 2591243: // Hilla
            case 2591134: // Von Leon
            case 2591227: // Hilla untradeable
            case 2591067: // Von Leon untradeable
                id = SoulPots.INT_15.getValue();
                break;
            case 2591212: // Arkarium
            case 2591299: // Black Knight (Elite) untradeable
            case 2591326: // Vicious Hunter (Elite) untradeable
            case 2591317: // Rampant Cyborg (Elite) untradeable
            case 2591335: // Bad Brawler (Elite) untradeable
            case 2591196: // Arkarium untradeable
            case 2591344: // Black Knight (Elite)
            case 2591368: // Vicious Hunter (Elite)
            case 2591360: // Rampant Cyborg (Elite)
            case 2591376: // Bad Brawler (Elite)
                id = SoulPots.INT_18.getValue();
                break;
            case 2591430: // Pierre
            case 2591385: // Pierre untradeable
            case 2591394: // Von Bon untradeable
            case 2591438: // Von Bon
            case 2591511: // Ursus
            case 2591520: // Ursus untradeable
            case 2591142: // Pink Bean
            case 2591057: // Pink Bean untradeable
            case 2591308: // Mad Mage (Elite) untradeable
            case 2591352: // Mad Mage (Elite)
                id = SoulPots.INT_20.getValue();
                break;
            case 2591181: // Cygnus
            case 2591077: // Cygnus untradeable
            case 2591274: // Magnus
            case 2591258: // Magnus untradeable
            case 2591290: // Murgoth
            case 2591403: // Crimson Queen untradeable
            case 2591446: // Crimson Queen
            case 2591412: // Vellum untradeable
            case 2591454: // Vellum
            case 2591421: // Lotus untradeable
            case 2591462: // Lotus
            case 2591566: // Damien untradeable
            case 2591575: // Damien
            case 2591584: // Lucid
                id = SoulPots.INT_24.getValue();
                break;

            // Fortuitous
            case 2591099: // Ani
            case 2591092: // Spirit of Rock
            case 2591268: // Black Slime
            case 2591167: // Xerxes
            case 2591020: // Ani untradeable
            case 2591013: // Spirit of Rock untradeable
            case 2591252: // Black slime untradeable
                id = SoulPots.LUK_3.getValue();
                break;
            case 2591190: // Ephenia untradeable
            case 2591206: // Ephenia
                id = SoulPots.LUK_4.getValue();
                break;
            case 2591106: // Dragon Rider
            case 2591034: // Rex untradeable
            case 2591027: // Dragon Rider untradeable
            case 2591113: // Rex
                id = SoulPots.LUK_5.getValue();
                break;
            case 2591221: // Pianus untradeable
            case 2591120: // Mu Gong
            case 2591237: // Pianus
            case 2591041: // Mu Gong untradeable
                id = SoulPots.LUK_7.getValue();
                break;
            case 2591127: // Balrog
            case 2591048: // Balrog untradeable
                id = SoulPots.LUK_10.getValue();
                break;
            case 2591158: // Zakum
            case 2591471: // Gold Dragon
            case 2591479: // Red Tiger
            case 2591174: // Zakum
                id = SoulPots.LUK_12.getValue();
                break;
            case 2591244: // Hilla
            case 2591135: // Von Leon
            case 2591228: // Hilla untradeable
            case 2591068: // Von Leon untradeable
                id = SoulPots.LUK_15.getValue();
                break;
            case 2591213: // Arkarium
            case 2591300: // Black Knight (Elite) untradeable
            case 2591327: // Vicious Hunter (Elite) untradeable
            case 2591309: // Mad Mage (Elite) untradeable
            case 2591336: // Bad Brawler (Elite) untradeable
            case 2591197: // Arkarium untradeable
            case 2591345: // Black Knight (Elite)
            case 2591353: // Mad Mage (Elite)
            case 2591369: // Vicious Hunter (Elite)
            case 2591377: // Bad Brawler (Elite)
                id = SoulPots.LUK_18.getValue();
                break;
            case 2591431: // Pierre
            case 2591386: // Pierre untradeable
            case 2591395: // Von Bon untradeable
            case 2591439: // Von Bon
            case 2591512: // Ursus
            case 2591521: // Ursus untradeable
            case 2591143: // Pink Bean
            case 2591058: // Pink Bean untradeable
            case 2591318: // Rampant Cyborg (Elite) untradeable
            case 2591361: // Rampant Cyborg (Elite)
                id = SoulPots.LUK_20.getValue();
                break;
            case 2591182: // Cygnus
            case 2591078: // Cygnus untradeable
            case 2591275: // Magnus
            case 2591259: // Magnus untradeable
            case 2591291: // Murgoth
            case 2591404: // Crimson Queen untradeable
            case 2591447: // Crimson Queen
            case 2591413: // Vellum untradeable
            case 2591455: // Vellum
            case 2591422: // Lotus untradeable
            case 2591463: // Lotus
            case 2591567: // Damien untradeable
            case 2591576: // Damien
            case 2591585: // Lucid
                id = SoulPots.LUK_24.getValue();
                break;

            // Flashy
            case 2591102: // Ani
            case 2591095: // Spirit of Rock
            case 2591271: // Black Slime
            case 2591170: // Xerxes
            case 2591209: // Ephenia
            case 2591193: // Ephenia untradeable
            case 2591023: // Ani untradeable
            case 2591016: // Spirit of Rock untradeable
            case 2591255: // Black slime untradeable
                id = SoulPots.ALLSTAT_2.getValue();
                break;
            case 2591109: // Dragon Rider
            case 2591037: // Rex untradeable
            case 2591030: // Dragon Rider untradeable
            case 2591116: // Rex
                id = SoulPots.ALLSTAT_3.getValue();
                break;
            case 2591224: // Pianus untradeable
            case 2591123: // Mu Gong
            case 2591240: // Pianus
            case 2591044: // Mu Gong untradeable
                id = SoulPots.ALLSTAT_5.getValue();
                break;
            case 2591128: // Balrog
            case 2591049: // Balrog untradeable
                id = SoulPots.ALLSTAT_7.getValue();
                break;
            case 2591159: // Zakum
            case 2591472: // Gold Dragon
            case 2591480: // Red Tiger
            case 2591175: // Zakum
                id = SoulPots.ALLSTAT_8.getValue();
                break;
            case 2591214: // Arkarium
            case 2591301: // Black Knight (Elite) untradeable
            case 2591328: // Vicious Hunter (Elite) untradeable
            case 2591310: // Mad Mage (Elite) untradeable
            case 2591319: // Rampant Cyborg (Elite) untradeable
            case 2591245: // Hilla
            case 2591136: // Von Leon
            case 2591229: // Hilla untradeable
            case 2591069: // Von Leon untradeable
            case 2591198: // Arkarium untradeable
            case 2591346: // Black Knight (Elite)
            case 2591354: // Mad Mage (Elite)
            case 2591370: // Vicious Hunter (Elite)
            case 25913562: // Rampant Cyborg (Elite)
                id = SoulPots.ALLSTAT_10.getValue();
                break;
            case 2591432: // Pierre
            case 2591387: // Pierre untradeable
            case 2591396: // Von Bon untradeable
            case 2591440: // Von Bon
            case 2591513: // Ursus
            case 2591522: // Ursus untradeable
            case 2591144: // Pink Bean
            case 2591059: // Pink Bean untradeable
            case 2591337: // Bad Brawler (Elite) untradeable
            case 2591378: // Bad Brawler (Elite)
                    id = SoulPots.ALLSTAT_12.getValue();
                break;
            case 2591183: // Cygnus
            case 2591079: // Cygnus untradeable
            case 2591276: // Magnus
            case 2591260: // Magnus untradeable
            case 2591292: // Murgoth
            case 2591405: // Crimson Queen untradeable
            case 2591448: // Crimson Queen
            case 2591414: // Vellum untradeable
            case 2591456: // Vellum
            case 2591423: // Lotus untradeable
            case 2591464: // Lotus
            case 2591568: // Damien untradeable
            case 2591577: // Damien
            case 2591586: // Lucid
                id = SoulPots.ALLSTAT_15.getValue();
                break;

            // Potent
            case 2591129: // Balrog
            case 2591050: // Balrog untradeable
                id = SoulPots.ATT_3.getValue();
                break;
            case 2591160: // Zakum
            case 2591473: // Gold Dragon
            case 2591481: // Red Tiger
            case 2591176: // Zakum
                id = SoulPots.ATT_3.getValue();
                break;
            case 2591215: // Arkarium
            case 2591302: // Black Knight (Elite) untradeable
            case 2591329: // Vicious Hunter (Elite) untradeable
            case 2591311: // Mad Mage (Elite) untradeable
            case 2591320: // Rampant Cyborg (Elite) untradeable
            case 2591338: // Bad Brawler (Elite) untradeable
            case 2591246: // Hilla
            case 2591137: // Von Leon
            case 2591230: // Hilla untradeable
            case 2591070: // Von Leon untradeable
            case 2591199: // Arkarium untradeable
            case 2591347: // Black Knight (Elite)
            case 2591355: // Mad Mage (Elite)
            case 2591371: // Vicious Hunter (Elite)
            case 25913563: // Rampant Cyborg (Elite)
            case 2591379: // Bad Brawler (Elite)
                id = SoulPots.ATT_4.getValue();
                break;
            case 2591433: // Pierre
            case 2591388: // Pierre untradeable
            case 2591397: // Von Bon untradeable
            case 2591441: // Von Bon
            case 2591514: // Ursus
            case 2591523: // Ursus untradeable
            case 2591145: // Pink Bean
            case 2591060: // Pink Bean untradeable
                id = SoulPots.ATT_5.getValue();
                break;
            case 2591184: // Cygnus
            case 2591080: // Cygnus untradeable
            case 2591277: // Magnus
            case 2591261: // Magnus untradeable
            case 2591293: // Murgoth
            case 2591406: // Crimson Queen untradeable
            case 2591449: // Crimson Queen
            case 2591415: // Vellum untradeable
            case 2591457: // Vellum
            case 2591424: // Lotus untradeable
            case 2591465: // Lotus
            case 2591569: // Damien untradeable
            case 2591578: // Damien
            case 2591587: // Lucid
                id = SoulPots.ATT_6.getValue();
                break;

            // Radiant
            case 2591130: // Balrog
            case 2591051: // Balrog untradeable
                id = SoulPots.MATT_3.getValue();
                break;
            case 2591161: // Zakum
            case 2591474: // Gold Dragon
            case 2591482: // Red Tiger
            case 2591177: // Zakum
                id = SoulPots.MATT_3.getValue();
                break;
            case 2591216: // Arkarium
            case 2591303: // Black Knight (Elite) untradeable
            case 2591330: // Vicious Hunter (Elite) untradeable
            case 2591312: // Mad Mage (Elite) untradeable
            case 2591321: // Rampant Cyborg (Elite) untradeable
            case 2591339: // Bad Brawler (Elite) untradeable
            case 2591247: // Hilla
            case 2591138: // Von Leon
            case 2591231: // Hilla untradeable
            case 2591071: // Von Leon untradeable
            case 2591200: // Arkarium untradeable
            case 2591348: // Black Knight (Elite)
            case 2591356: // Mad Mage (Elite)
            case 25913671: // Vicious Hunter (Elite)
            case 25913564: // Rampant Cyborg (Elite)
            case 2591380: // Bad Brawler (Elite)
                id = SoulPots.MATT_4.getValue();
                break;
            case 2591434: // Pierre
            case 2591389: // Pierre untradeable
            case 2591398: // Von Bon untradeable
            case 2591442: // Von Bon
            case 2591515: // Ursus
            case 2591524: // Ursus untradeable
            case 2591146: // Pink Bean
            case 2591061: // Pink Bean untradeable
                id = SoulPots.MATT_5.getValue();
                break;
            case 2591185: // Cygnus
            case 2591081: // Cygnus untradeable
            case 2591278: // Magnus
            case 2591262: // Magnus untradeable
            case 2591294: // Murgoth
            case 2591407: // Crimson Queen untradeable
            case 2591450: // Crimson Queen
            case 2591416: // Vellum untradeable
            case 2591458: // Vellum
            case 2591425: // Lotus untradeable
            case 2591466: // Lotus
            case 2591570: // Damien untradeable
            case 2591579: // Damien
            case 2591588: // Lucid
                id = SoulPots.MATT_6.getValue();
                break;

            // Hearty
            case 2591100: // Ani
            case 2591093: // Spirit of Rock
            case 2591269: // Black Slime
            case 2591168: // Xerxes
            case 2591021: // Ani untradeable
            case 2591014: // Spirit of Rock untradeable
            case 2591253: // Black slime untradeable
                id = SoulPots.MHP_150.getValue();
                break;
            case 2591191: // Ephenia untradeable
            case 2591207: // Ephenia
                id = SoulPots.MHP_180.getValue();
                break;
            case 2591107: // Dragon Rider
            case 2591035: // Rex untradeable
            case 2591028: // Dragon Rider untradeable
            case 2591114: // Rex
                id = SoulPots.MHP_200.getValue();
                break;
            case 2591222: // Pianus untradeable
            case 2591121: // Mu Gong
            case 2591238: // Pianus
            case 2591042: // Mu Gong untradeable
                id = SoulPots.MHP_300.getValue();
                break;
            case 2591131: // Balrog
            case 2591052: // Balrog untradeable
                id = SoulPots.MHP_400.getValue();
                break;
            case 2591162: // Zakum
            case 2591475: // Gold Dragon
            case 2591483: // Red Tiger
            case 2591178: // Zakum
                id = SoulPots.MHP_500.getValue();
                break;
            case 2591248: // Hilla
            case 2591139: // Von Leon
            case 2591232: // Hilla untradeable
            case 2591072: // Von Leon untradeable
                id = SoulPots.MHP_600.getValue();
                break;
            case 2591217: // Arkarium
            case 2591304: // Black Knight (Elite) untradeable
            case 2591331: // Vicious Hunter (Elite) untradeable
            case 2591313: // Mad Mage (Elite) untradeable
            case 2591322: // Rampant Cyborg (Elite) untradeable
            case 2591340: // Bad Brawler (Elite) untradeable
            case 2591201: // Arkarium untradeable
            case 2591349: // Black Knight (Elite)
            case 25913672: // Vicious Hunter (Elite)
            case 25913565: // Rampant Cyborg (Elite)
            case 2591381: // Bad Brawler (Elite)
                id = SoulPots.MHP_700.getValue();
                break;
            case 2591435: // Pierre:
            case 2591390: // Pierre untradeable
            case 2591399: // Von Bon untradeable
            case 2591443: // Von Bon
            case 2591516: // Ursus
            case 2591525: // Ursus untradeable
            case 2591147: // Pink Bean
            case 2591062: // Pink Bean untradeable
                id = SoulPots.MHP_800.getValue();
                break;
            case 2591186: // Cygnus
            case 2591082: // Cygnus untradeable
            case 2591279: // Magnus
            case 2591263: // Magnus untradeable
            case 2591295: // Murgoth
            case 2591408: // Crimson Queen untradeable
            case 2591451: // Crimson Queen
            case 2591417: // Vellum untradeable
            case 2591459: // Vellum
            case 2591426: // Lotus untradeable
            case 2591467: // Lotus
            case 2591571: // Damien untradeable
            case 2591580: // Damien
            case 2591589: // Lucid
                id = SoulPots.MHP_960.getValue();
                break;

            // Ample
            case 2591101: // Ani
            case 2591094: // Spirit of Rock
            case 2591270: // Black Slime
            case 2591169: // Xerxes
            case 2591022: // Ani untradeable
            case 2591015: // Spirit of Rock untradeable
            case 2591254: // Black slime untradeable
                id = SoulPots.MMP_150.getValue();
                break;
            case 2591192: // Ephenia untradeable
            case 2591208: // Ephenia
                id = SoulPots.MMP_180.getValue();
                break;
            case 2591108: // Dragon Rider
            case 2591036: // Rex untradeable
            case 2591029: // Dragon Rider untradeable
            case 2591115: // Rex
                id = SoulPots.MMP_200.getValue();
                break;
            case 2591223: // Pianus untradeable
            case 2591122: // Mu Gong
            case 2591239: // Pianus
            case 2591043: // Mu Gong untradeable
                id = SoulPots.MMP_300.getValue();
                break;

            // Magnificent
            case 2591085: // Balrog
                id = MagnificentSoulOptions.TIER_1.getRandomOption();
                break;
            case 2591163: // Zakum
            case 2591484: // Gold Dragon
            case 2591485: // Red Tiger
                id = MagnificentSoulOptions.TIER_2.getRandomOption();
                break;
            case 2591086: // Von Leon
                id = MagnificentSoulOptions.TIER_3.getRandomOption();
                break;
            case 2591233: // Hilla
            case 2591202: // Arkarium
                id = MagnificentSoulOptions.TIER_4.getRandomOption();
                break;
            case 2591305: // Black Knight (Elite)
                id = MagnificentSoulOptions.BLACK_KNIGHT.getRandomOption();
                break;
            case 2591332: // Vicious Hunter (Elite)
                id = MagnificentSoulOptions.VICIOUS_HUNTER.getRandomOption();
                break;
            case 2591314: // Mad Mage (Elite)
                id = MagnificentSoulOptions.MAD_MAGE.getRandomOption();
                break;
            case 2591323: // Rampant Cyborg (Elite)
                id = MagnificentSoulOptions.RAMPANT_CYBORG.getRandomOption();
                break;
            case 2591341: // Bad Brawler (Elite)
                id = MagnificentSoulOptions.BAD_BRAWLER.getRandomOption();
                break;
            case 2591486: // Pink bean untradeable
            case 2591087: // Pink bean
            case 2591517: // Ursus
            case 2591400: // Von Bon
            case 2591391: // Pierre
                id = MagnificentSoulOptions.TIER_5.getRandomOption();
                break;
            case 2591264: // Magnus
            case 2591088: // Cygnus
            case 2591296: // Murgoth
            case 2591409: // Crimson Queen
            case 2591418: // Vellum
            case 2591427: // Lotus
            case 2591581: // Damien
            case 2591572: // Damien
            case 2591590: // Lucid
                id = MagnificentSoulOptions.TIER_6.getRandomOption();
                break;
            default:
                id = MagnificentSoulOptions.TIER_1.getRandomOption();
                break;
        }
        return id;
    }

    public static int getSoulSkillFromSoulID(int soulID) {
        switch(soulID) {
            case 256:
            case 257:
            case 258:
            case 259:
            case 260:
            case 261:
            case 262:
            case 263:
                return 80001340; // Advance of Magnus
        }
        return 0;
    }

    public static boolean isMobCard(int itemID) {
        return getItemPrefix(itemID) == 238;
    }

    public static boolean isCollisionLootItem(int itemID) {
        switch (itemID) {
            case 2023484: // Blue
            case 2023494: // Purple
            case 2023495: // Red
            case 2023669: // Gold
                return true;

            default:
                return false;
        }
    }

    public static boolean isUpgradable(int itemID) {
        BodyPart bodyPart = BodyPart.getByVal(getBodyPartFromItem(itemID, 0));
        if (bodyPart == null || getItemPrefix(itemID) == EquipPrefix.SecondaryWeapon.getVal()) {
            return false;
        }
        switch (bodyPart) {
            case Ring1:
            case Ring2:
            case Ring3:
            case Ring4:
            case Pendant:
            case ExtendedPendant:
            case Weapon:
            case Belt:
            case Hat:
            case FaceAccessory:
            case EyeAccessory:
            case Top:
            case Bottom:
            case Shoes:
            case Earrings:
            case Shoulder:
            case Gloves:
            case Badge:
            case Shield:
            case Cape:
            case MechanicalHeart:
                return true;
            default:
                return false;
        }
    }

    public static List<ScrollUpgradeInfo> getScrollUpgradeInfosByEquip(Equip equip) {
        // not the most beautiful way to do this, but I'd like to think that it's pretty easy to understand
        BodyPart bp = BodyPart.getByVal(ItemConstants.getBodyPartFromItem(equip.getItemId(), 0));
        List<ScrollUpgradeInfo> scrolls = new ArrayList<>();
        int rLevel = equip.getrLevel() + equip.getiIncReq();
        int rJob = equip.getrJob();
        Set<EnchantStat> possibleStat = new HashSet<>();
        int plusFromLevel;
        int[] chances;
        int[] attStats = new int[0];
        int[] stat;
        int[] armorHp = new int[]{5, 20, 30, 70, 120};
        int[] armorDef = new int[]{1, 2, 4, 7, 10};
        boolean armor = false;
        if (bp == BodyPart.Weapon) {
            plusFromLevel = rLevel >= 120 ? 2 : rLevel >= 60 ? 1 : 0;
            if ((rJob & RequiredJob.Warrior.getVal()) > 0) { // warrior
                possibleStat.add(EnchantStat.PAD);
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.MHP);
            } else if ((rJob & RequiredJob.Magician.getVal()) > 0) { // mage
                possibleStat.add(EnchantStat.MAD);
                possibleStat.add(EnchantStat.INT);
            } else if ((rJob & RequiredJob.Bowman.getVal()) > 0) { // bowman
                possibleStat.add(EnchantStat.PAD);
                possibleStat.add(EnchantStat.DEX);
            } else if ((rJob & RequiredJob.Thief.getVal()) > 0 || (rJob & RequiredJob.Pirate.getVal()) > 0) { // thief/pirate
                possibleStat.add(EnchantStat.PAD);
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.LUK);
            } else {
                possibleStat.add(EnchantStat.PAD);
                possibleStat.add(EnchantStat.MAD);
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.INT);
                possibleStat.add(EnchantStat.LUK);
                possibleStat.add(EnchantStat.MHP);
            }
            chances = new int[]{100, 70, 30, 15};
            attStats = new int[]{1, 2, 3, 5, 7, 9};
            stat = new int[]{0, 0, 1, 2, 3, 4};
        } else if (bp == BodyPart.Gloves) {
            plusFromLevel = rLevel <= 70 ? 0 : 1;
            if ((rJob & RequiredJob.Magician.getVal()) > 0) {
                possibleStat.add(EnchantStat.MAD);
            } else {
                possibleStat.add(EnchantStat.PAD);
            }
            possibleStat.add(EnchantStat.PDD);
            possibleStat.add(EnchantStat.MDD);
            chances = new int[]{100, 70, 30};
            attStats = new int[]{0, 1, 2, 3};
            stat = new int[]{3, 0, 0, 0};
        } else if (ItemConstants.isAccessory(equip.getItemId())) {
            plusFromLevel = rLevel >= 120 ? 2 : rLevel >= 60 ? 1 : 0;
            if ((rJob & RequiredJob.Warrior.getVal()) > 0) { // warrior
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.MHP);
            } else if ((rJob & RequiredJob.Magician.getVal()) > 0) { // mage
                possibleStat.add(EnchantStat.INT);
            } else if ((rJob & RequiredJob.Bowman.getVal()) > 0) { // bowman
                possibleStat.add(EnchantStat.DEX);
            } else if ((rJob & RequiredJob.Thief.getVal()) > 0 || (rJob & RequiredJob.Pirate.getVal()) > 0) { // thief/pirate
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.LUK);
            } else {
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.INT);
                possibleStat.add(EnchantStat.LUK);
                possibleStat.add(EnchantStat.MHP);
            }
            chances = new int[]{100, 70, 30};
            stat = new int[]{1, 1, 2, 3, 5};
        } else {
            armor = true;
            plusFromLevel = rLevel >= 120 ? 2 : rLevel >= 60 ? 1 : 0;
            if ((rJob & RequiredJob.Warrior.getVal()) > 0) { // warrior
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.MHP);
            } else if ((rJob & RequiredJob.Magician.getVal()) > 0) { // mage
                possibleStat.add(EnchantStat.INT);
            } else if ((rJob & RequiredJob.Bowman.getVal()) > 0) { // bowman
                possibleStat.add(EnchantStat.DEX);
            } else if ((rJob & RequiredJob.Thief.getVal()) > 0 || (rJob & RequiredJob.Pirate.getVal()) > 0) { // thief/pirate
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.LUK);
            } else {
                possibleStat.add(EnchantStat.STR);
                possibleStat.add(EnchantStat.DEX);
                possibleStat.add(EnchantStat.INT);
                possibleStat.add(EnchantStat.LUK);
                possibleStat.add(EnchantStat.MHP);
            }
            chances = new int[]{100, 70, 30};
            stat = new int[]{1, 2, 3, 5, 7};
        }
        for (int i = 0; i < chances.length; i++) { // 4 scroll tiers for weapons
            int tier = i + plusFromLevel;
            TreeMap<EnchantStat, Integer> stats = new TreeMap<>();
            for (EnchantStat es : possibleStat) {
                int val;
                if (es.isAttackType()) {
                    val = attStats[tier];
                } else if (es.isHpOrMp()){
                    val = stat[tier] * 50;
                } else {
                    val = stat[tier];
                }
                if (val != 0) {
                    stats.put(es, val);
                }
            }
            if (armor) {
                stats.put(EnchantStat.PDD, armorDef[tier] + stats.getOrDefault(EnchantStat.PDD, 0));
                stats.put(EnchantStat.MDD, armorDef[tier] + stats.getOrDefault(EnchantStat.MDD, 0));
                stats.put(EnchantStat.MHP, armorHp[tier] + stats.getOrDefault(EnchantStat.MHP, 0));
            }
            String title = chances[i] + "% ";
            title += bp == BodyPart.Weapon ? "Attack" : "Stat";
            ScrollUpgradeInfo sui = new ScrollUpgradeInfo(i, title, SpellTraceScrollType.Normal, 0, stats,
                    BASE_ST_COST + rLevel * (tier + 1), chances[i]);
            scrolls.add(sui);
        }
        if (equip.hasUsedSlots()) {
            scrolls.add(new ScrollUpgradeInfo(4, "Innocence Scroll 30%",
                    SpellTraceScrollType.Innocence, 0, new TreeMap<>(), INNOCENCE_ST_COST, 30));
            scrolls.add(new ScrollUpgradeInfo(5, "Clean Slate Scroll 5%",
                    SpellTraceScrollType.CleanSlate, 0, new TreeMap<>(), CLEAN_SLATE_ST_COST, 5));
        }
        return scrolls;
    }

    // is_tuc_ignore_item(int nItemID)
    static boolean isTucIgnoreItem(int itemID) {
        return (isSecondary(itemID) || isEmblem(itemID) || Arrays.asList(TUC_IGNORE_ITEMS).contains(itemID));
    }

    public static PetSkill getPetSkillFromID(int itemID) {
        switch (itemID) {
            case 5190000:
                return PetSkill.ITEM_PICKUP;
            case 5190001:
                return PetSkill.AUTO_HP;
            case 5190002:
                return PetSkill.EXPANDED_AUTO_MOVE;
            case 5190003:
                return PetSkill.AUTO_MOVE;
            case 5190004:
                return PetSkill.EXPIRED_PICKUP;
            case 5190005:
                return PetSkill.IGNORE_ITEM;
            case 5190006:
                return PetSkill.AUTO_MP;
            case 5190007:
                return PetSkill.RECALL;
            case 5190008:
                return PetSkill.AUTO_SPEAKING;
            case 5190009:
                return PetSkill.AUTO_ALL_CURE;
            case 5190010:
                return PetSkill.AUTO_BUFF;
            case 5190011:
                return PetSkill.AUTO_FEED;
            case 5190012:
                return PetSkill.FATTEN_UP;
            case 5190013:
                return PetSkill.PET_SHOP;
            case 5190014:
                return PetSkill.FATTEN_UP;
            case 5191000:
                return PetSkill.ITEM_PICKUP;
            case 5191001:
                return PetSkill.AUTO_HP;
            case 5191002:
                return PetSkill.EXPANDED_AUTO_MOVE;
            case 5191003:
                return PetSkill.ITEM_PICKUP;
        }
        return null;
    }

    // Gets the hardcoded starforce capacities Nexon introduced for equips above level 137.
    // The cap for stars is in GetHyperUpgradeCapacity (E8 ? ? ? ? 0F B6 CB 83 C4 0C, follow `call`),
    // therefore it needs to be manually implemented on the server side.
    // Nexon's decision was very poor, but will require client edits to revert.
    static int getItemStarLimit(int itemID) {
        switch (itemID) {
            case 1072870: // Sweetwater Shoes
            case 1082556: // Sweetwater Gloves
            case 1102623: // Sweetwater Cape
            case 1132247: // Sweetwater Belt
                if (ServerConstants.VERSION >= 197) {
                    return 15;
                }
            case 1182060: // Ghost Ship Exorcist
            case 1182273: // Sengoku Hakase Badge
                if (ServerConstants.VERSION >= 199) {
                    return 22;
                }
        }
        return ServerConstants.VERSION >= 197 ? 25 : 15;
    }

    public static int getEquippedSummonSkillItem(int itemID, short job) {
        switch (itemID) {
            case 1112585:// Angelic Blessing
                return (SkillConstants.getNoviceSkillRoot(job) * 10000) + 1085;
            case 1112586:// Dark Angelic Blessing
                return (SkillConstants.getNoviceSkillRoot(job) * 10000) + 1087;
            case 1112594:// Snowdrop Angelic Blessing
                return (SkillConstants.getNoviceSkillRoot(job) * 10000) + 1090;
            case 1112663:// White Angelic Blessing
                return (SkillConstants.getNoviceSkillRoot(job) * 10000) + 1179;
            case 1112735:// White Angelic Blessing 2
                return 80001154;
            case 1113020:// Lightning God Ring
                return 80001262;
            case 1113173:// Lightning God Ring 2
                return 80011178;
            // Heaven Rings
            case 1112932:// Guard Ring
                return 80011149;
            case 1114232:// Sun Ring
                return 80010067;
            case 1114233:// Rain Ring
                return 80010068;
            case 1114234:// Rainbow Ring
                return 80010069;
            case 1114235:// Snow Ring
                return 80010070;
            case 1114236:// Lightning Ring
                return 80010071;
            case 1114237:// Wind Ring
                return 80010072;
        }
        return 0;
    }

    public static boolean isRecipeOpenItem(int itemID) {
        return itemID / 10000 == 251;
    }

    public static Set<DropInfo> getConsumableMobDrops(int level) {
        level = Math.min(100, (level / 20) * 20); // round it to the nearest 20th level + max of level 100
        return consumableDropsPerLevel.getOrDefault(level, new HashSet<>());
    }

    public static Set<DropInfo> getEquipMobDrops(short job, int level) {
        level = Math.min(140, (level / 10) * 10); // round it to the nearest 10th level + max of level 140
        ItemJob itemJob = GameConstants.getItemJobByJob(job);
        if (itemJob == null) {
            itemJob = ItemJob.BEGINNER;
        }
        return equipDropsPerLevel.getOrDefault(itemJob, new HashMap<>()).getOrDefault(level, new HashSet<>());
    }

    public static boolean isMiuMiuMerchant(int itemID) {
        return itemID == 5450000 || itemID == 5450003 || itemID == 5450007 || itemID == 5450012 || itemID == 5450013 || itemID == 5450004;
    }

    public static boolean isPortableStorage(int itemID) {
        return itemID == 5450009 || itemID == 5450008 || itemID == 5450005;
    }

    public static boolean is2XDropCoupon(int itemId) {
        return itemId <= 5360057 && itemId >= 5360000;
    }
}

